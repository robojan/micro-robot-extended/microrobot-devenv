#!/bin/bash


HOSTFILE_DIR="/ssh_hostfiles"

# Generate SSH host keys if they don't exist
if [ ! -f ${HOSTFILE_DIR}/ssh_host_rsa_key ]; then
    ssh-keygen -t ecdsa -f ${HOSTFILE_DIR}/ssh_host_ecdsa_key -N ""
    ssh-keygen -t ed25519 -f ${HOSTFILE_DIR}/ssh_host_ed25519_key -N ""
    ssh-keygen -t rsa -f ${HOSTFILE_DIR}/ssh_host_rsa_key -N ""
fi

# Forward certain environement variables to the SSH sessions
FORWARDED_ENV_VARS="DBUS_SESSION_BUS_ADDRESS DBUS_SYSTEM_BUS_ADDRESS"
for VAR in ${FORWARDED_ENV_VARS}; do
    if [ -n "${!VAR}" ]; then
        echo "Forwarding ${VAR}=${!VAR} to SSH sessions"
        echo "export ${VAR}=${!VAR}" >> /home/user/.profile
    fi
done

# start sshd
SSH_PORT=2222
SSH_COMMAND=$(which sshd)
${SSH_COMMAND} -D -e -h ${HOSTFILE_DIR}/ssh_host_ecdsa_key -h ${HOSTFILE_DIR}/ssh_host_ed25519_key -h${HOSTFILE_DIR}/ssh_host_rsa_key \
    -p $SSH_PORT