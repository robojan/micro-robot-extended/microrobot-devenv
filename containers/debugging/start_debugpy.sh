#!/bin/bash

STAGING_DIR=/home/user/staging
PORT=${1}
shift # Discard the first argument

# Source the environment
source $HOME/.profile

echo "Starting debugpy"

export LD_LIBRARY_PATH="${STAGING_DIR}/lib:/home/user/miniforge3/envs/ros_env/lib:${LD_LIBRARY_PATH}"
export AMENT_PREFIX_PATH="${STAGING_DIR}:${AMENT_PREFIX_PATH}"
export PYTHONPATH="${STAGING_DIR}/lib/python3.11/site-packages:${PYTHONPATH}"

python -m debugpy --listen 0.0.0.0:${PORT} --wait-for-client --log-to-stderr ${@}

echo "debugpy stopped"

