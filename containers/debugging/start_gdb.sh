#!/bin/bash

STAGING_DIR=/home/user/staging
EXECUTABLE=${STAGING_DIR}/${1}
shift # Discard the first argument

# Source the environment
source $HOME/.profile

echo "Starting gdbserver"

export LD_LIBRARY_PATH="${STAGING_DIR}/lib:/home/user/miniforge3/envs/ros_env/lib:${LD_LIBRARY_PATH}"
export AMENT_PREFIX_PATH="${STAGING_DIR}:${AMENT_PREFIX_PATH}"

gdbserver --once :3000 $EXECUTABLE ${@}

echo "gdbserver stopped"

