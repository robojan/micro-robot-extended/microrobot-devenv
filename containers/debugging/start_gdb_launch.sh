#!/bin/bash

STAGING_DIR=/home/user/staging
LAUNCH_FILE=${1:-"/launch.yaml"}
ROS2=$(which ros2)
PYTHON=$(which python)

# Source the environment
source $HOME/.profile

echo "Starting gdbserver"

export LD_LIBRARY_PATH="${STAGING_DIR}/lib:/home/user/miniforge3/envs/ros_env/lib:${LD_LIBRARY_PATH}"
export AMENT_PREFIX_PATH="${STAGING_DIR}:${AMENT_PREFIX_PATH}"

gdbserver --once :3000 $PYTHON $ROS2 launch $LAUNCH_FILE
# gdb --args $PYTHON $ROS2 launch $LAUNCH_FILE

echo "gdbserver stopped"

