#!/bin/bash -e

LAUNCH_ARGS=""

# Set the ports if they are overridden
if [ ! -z "$ROSBRIDGE_PORT" ]; then
    LAUNCH_ARGS="$LAUNCH_ARGS rosbridge_port:=$ROSBRIDGE_PORT"
fi
if [ ! -z "$FOXGLOVE_PORT" ]; then
    LAUNCH_ARGS="$LAUNCH_ARGS foxglove_port:=$FOXGLOVE_PORT"
fi
if [ ! -z "$SSH_PORT" ]; then
    LAUNCH_ARGS="$LAUNCH_ARGS ssh_port:=$SSH_PORT"
fi
if [ ! -z "$HTTP_PORT" ]; then
    LAUNCH_ARGS="$LAUNCH_ARGS http_port:=$HTTP_PORT"
fi
if [ ! -z "$ZENOH_PORT" ]; then
    LAUNCH_ARGS="$LAUNCH_ARGS zenoh_port:=$ZENOH_PORT"
fi

# Set the robot identification
# The identification should be stored in a file in /robot_mfg_data/robot_info.json
# Warn if the file is not found, else set the type and serial from this file
if [ ! -f "/robot_mfg_data/robot_info.json" ]; then
    echo -e "\033[33mWARNING: Robot info file not found at /robot_mfg_data/robot_info.json\033[0m"
else
    ROBOT_TYPE=$(jq -r '.type' /robot_mfg_data/robot_info.json)
    ROBOT_SERIAL=$(jq -r '.serial' /robot_mfg_data/robot_info.json)
    LAUNCH_ARGS="$LAUNCH_ARGS robot_type:=\"$ROBOT_TYPE\" robot_serial:=\"$ROBOT_SERIAL\""
fi

# Set the robot name
# The name should be stored in a file in /robot_user_data/robot_name.txt
# Warn if the file is not found, else set the name from this file
if [ ! -f "/robot_user_data/robot_name.txt" ]; then
    echo -e "\033[33mWARNING: Robot name file not found at /robot_user_data/robot_name.txt\033[0m"
else
    ROBOT_NAME=$(cat /robot_user_data/robot_name.txt)
    LAUNCH_ARGS="$LAUNCH_ARGS robot_name:=\"$ROBOT_NAME\""
fi

ros2 launch /launch.yaml $LAUNCH_ARGS
