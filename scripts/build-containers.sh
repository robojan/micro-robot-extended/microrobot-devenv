#!/bin/bash -e

IMAGE_REPO="registry.gitlab.com/robojan/micro-robot-extended/microrobot-devenv"
IMAGE_TAG="latest"
IMAGES="base base-dev drivers remote-interface debugging"

# Create a mapping of the images to their base image
declare -A BASE_IMAGES
BASE_IMAGES[base-dev]="base"
BASE_IMAGES[drivers]="base"
BASE_IMAGES[remote-interface]="base"
BASE_IMAGES[debugging]="base"

# Create a list of platforms for the images
declare -A PLATFORMS
PLATFORMS[base]="linux/amd64,linux/arm64"
PLATFORMS[base-dev]="linux/amd64"
PLATFORMS[drivers]="linux/amd64,linux/arm64"
PLATFORMS[remote-interface]="linux/amd64,linux/arm64"
PLATFORMS[debugging]="linux/amd64,linux/arm64"

# Parse the command line options
CACHE_OPTION="--no-cache"
DO_UPLOAD="true"
while [[ $# -gt 0 ]]; do
    case "$1" in
        --help)
            echo "Usage: ./build.sh [OPTIONS]"
            echo "Options:"
            echo "  --help: Print this help message"
            echo "  --tag: Set the image tag (default: ${IMAGE_TAG}})"
            echo "  --repo: Set the image repository (default: ${IMAGE_REPO})"
            echo "  --with-cache: Use the cache when building the images"
            echo "  --images: Set the images to build (default: ${IMAGES})"
            echo "  --no-upload: Do not upload the images"
            exit 0
            ;;
        --tag)
            IMAGE_TAG="$2"
            shift
            ;;
        --repo)
            IMAGE_REPO="$2"
            shift
            ;;
        --with-cache)
            CACHE_OPTION=""
            ;;
        --images)
            IMAGES="$2"
            shift
            ;;
        --conda-repo-host)
            CONDA_REPO_HOST="$2"
            shift
            ;;
        --no-upload)
            DO_UPLOAD="false"
            ;;
        *)
            echo "Unknown option: $1"
            exit 1
            ;;
    esac
    shift
done

# Split the platforms and images into an array based on spaces or commas
IFS=', ' read -r -a IMAGES <<< "${IMAGES}"

# Error if no images are specified
if [[ -z "${IMAGES}" ]]; then
    echo "No images specified"
    exit 1
fi

SCRIPTS_DIR=$(dirname $(realpath $BASH_SOURCE))
ROOT_DIR=$(realpath ${SCRIPTS_DIR}/..)

# Build the images
for image in "${IMAGES[@]}"; do
    echo "Building ${image}"

    # Build the images for each platform
    platform_images=""

    # Set the build args
    build_args=""
    if [[ -n "${BASE_IMAGES[${image}]}" ]]; then
        build_args="--build-arg BASE_IMAGE=${IMAGE_REPO}/${BASE_IMAGES[${image}]}:${IMAGE_TAG}"
    fi
    build_args="${build_args} --platform ${PLATFORMS[${image}]}"
    if [ "a$DO_UPLOAD" == "atrue" ]; then
        build_args="${build_args} --push"
    fi

    # Build the image
    echo "Running docker buildx build --tag ${IMAGE_REPO}/${image}:${IMAGE_TAG}  \
        ${build_args} ${CACHE_OPTION} --file $ROOT_DIR/containers/${image}/Dockerfile $ROOT_DIR/containers"
    docker buildx build \
        --tag ${IMAGE_REPO}/${image}:${IMAGE_TAG} \
        ${build_args} ${CACHE_OPTION} \
         --file $ROOT_DIR/containers/${image}/Dockerfile $ROOT_DIR/containers
done
