#!/bin/bash

set -e

PACKAGES="microrobot_description microrobot_interfaces microrobot_ll microrobot_system_manager marker_localization"

SCRIPTS_DIR=$(realpath $(dirname $BASH_SOURCE))
WORKSPACE_DIR=$(realpath $SCRIPTS_DIR/..)
ARCHIVES_DIR=$WORKSPACE_DIR/containers/archives

usage() {
    echo "Usage: $0 [-h] INSTALL_DIR ARCH"
    echo "Create archives for the microrobot packages."
    echo "  -h  display this help and exit"
    echo "  INSTALL_DIR  directory where the build and installed files are located"
    echo "  ARCH  architecture of the target system"
}

POSITIONAL_ARGS=()
while [ $# -gt 0 ]; do
    key="$1"
    case $key in
        -h|--help)
            usage
            exit 0
            ;;
        *)
            POSITIONAL_ARGS+=("$1")
            shift
            ;;
    esac
done

set -- "${POSITIONAL_ARGS[@]}"
if [ $# -ne 2 ]; then
    usage
    exit 1
fi

INSTALL_DIR=$(realpath $1)
ARCH=$2

OUTPUT_DIR=$ARCHIVES_DIR/$ARCH

mkdir -p $OUTPUT_DIR
for package in $PACKAGES; do
    echo "Creating archive for $package..."
    tar -czf $OUTPUT_DIR/$package.tar.gz -C $INSTALL_DIR/$package .
done
