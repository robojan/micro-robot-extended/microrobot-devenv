#!/bin/bash

set -e

ROOTFS_BASE=/rootfs
SCRIPTS_DIR=$(realpath $(dirname $BASH_SOURCE))

pushd $ROOTFS_BASE

# Download apt-keys
mkdir -p apt-keys
rm -f apt-keys/ubuntu-archive-keyring.gpg apt-keys/ros-archive-keyring.gpg

# Copy over the ubuntu keyring
cp /usr/share/keyrings/ubuntu-archive-keyring.gpg apt-keys/

# Download the ROS keyring
curl -sSL https://raw.githubusercontent.com/ros/rosdistro/master/ros.key | gpg --dearmor > apt-keys/ros-archive-keyring.gpg

# Create the rootfs directory
rm -rf microrobot
bdebstrap --config $SCRIPTS_DIR/microrobot_rootfs.yaml --name microrobot --target $ROOTFS_BASE/microrobot/rootfs

# Copy the cmake_toolchain_file to the rootfs
mmdebstrap --unshare-helper ln -s $SCRIPTS_DIR/microrobot_toolchain.cmake microrobot/rootfs/toolchain.cmake
mmdebstrap --unshare-helper chmod 0666 microrobot/rootfs/toolchain.cmake
