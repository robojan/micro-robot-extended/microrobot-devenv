# CMake toolchain file for the microrobot

include_guard(GLOBAL)

set(CMAKE_SYSTEM_NAME Linux)
set(CMAKE_SYSTEM_PROCESSOR aarch64)

message(STATUS "Cross compiling for the microrobot")

# Set the rootfs path
set(ROOTFS_PATH ${CMAKE_CURRENT_LIST_DIR})

# Find the cross compiler
find_program(CMAKE_C_COMPILER aarch64-linux-gnu-gcc)
find_program(CMAKE_CXX_COMPILER aarch64-linux-gnu-g++)
find_program(CMAKE_AR aarch64-linux-gnu-ar)
find_program(CMAKE_RANLIB aarch64-linux-gnu-ranlib)
find_program(CMAKE_NM aarch64-linux-gnu-nm)
find_program(CMAKE_OBJCOPY aarch64-linux-gnu-objcopy)
find_program(CMAKE_OBJDUMP aarch64-linux-gnu-objdump)
find_program(CMAKE_STRIP aarch64-linux-gnu-strip)

# Get the root of installed packages, but exclude /opt/ros/humble
set(INSTALLED_PKGS_PREFIX $ENV{CMAKE_PREFIX_PATH})
string(REPLACE ":" ";" INSTALLED_PKGS_PREFIX "${INSTALLED_PKGS_PREFIX}")
list(FILTER INSTALLED_PKGS_PREFIX EXCLUDE REGEX "/opt/ros/humble")
string(REPLACE ";" ":" INSTALLED_PKGS_PREFIX_NATIVE "${INSTALLED_PKGS_PREFIX}")

# Make sure that we can find dependencies
set(CMAKE_SYSROOT "${ROOTFS_PATH}")
set(CMAKE_FIND_ROOT_PATH "${ROOTFS_PATH};${ROOTFS_PATH}/opt/ros/humble;${INSTALLED_PKGS_PREFIX}")
set(CMAKE_FIND_ROOT_PATH_MODE_PROGRAM NEVER)
set(CMAKE_FIND_ROOT_PATH_MODE_LIBRARY ONLY)
set(CMAKE_FIND_ROOT_PATH_MODE_INCLUDE ONLY)
set(CMAKE_FIND_ROOT_PATH_MODE_PACKAGE ONLY)

# Enable emulation with Qemu, Tell the linker to use the sysroot
# Add the installed packages to the AMENT_PREFIX_PATH
set(INSTALLED_PKGS_AMENT_PREFIX $ENV{AMENT_PREFIX_PATH})
string(REPLACE ":/opt/ros/humble" "" INSTALLED_PKGS_AMENT_PREFIX "${INSTALLED_PKGS_AMENT_PREFIX}")
set(emulating_env "AMENT_PREFIX_PATH=${ROOTFS_PATH}/opt/ros/humble:${INSTALLED_PKGS_AMENT_PREFIX}")
set(emulating_env "${emulating_env},LD_LIBRARY_PATH=/opt/ros/humble/lib:/opt/ros/humble/lib/aarch64-linux-gnu")
set(CMAKE_CROSSCOMPILING_EMULATOR "/usr/bin/qemu-aarch64-static;-L;${ROOTFS_PATH};-E;${emulating_env}")

# Set optimization flags for the jetson orin NX
set(CMAKE_C_FLAGS "${CMAKE_C_FLAGS} -march=armv8.2-a+fp16")
set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -march=armv8.2-a+fp16")

# Set the python ABI
set(PYTHON_SOABI "cpython-310-aarch64-linux-gnu")
