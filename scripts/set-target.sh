#!/bin/bash -e

# This script sets the hostname target_robot.internal to the specified IP address
# This is used to allow the robot to be accessed by name from the host machine

# Parse the command line options
while [[ $# -gt 0 ]]; do
    case "$1" in
        --help)
            echo "Usage: ./set_target.sh [OPTIONS] ip"
            echo "Options:"
            echo "  --help: Print this help message"
            echo "  ip: Set the IP address to set the target to"
            exit 0
            ;;
        *)
            ip=$1
            ;;
    esac
    shift
done

if [ -z "$ip" ]; then
    echo "No IP address specified"
    exit 1
fi

# If the IP address is not valid, assume it is a hostname
# try to resolve it
if [[ ! $ip =~ ^[0-9]+\.[0-9]+\.[0-9]+\.[0-9]+$ ]]; then
    ip=$(getent hosts $ip | awk '{ print $1 }')
    if [ -z "$ip" ]; then
        echo "Could not resolve $ip"
        exit 1
    fi
fi

# Set the target
echo "Setting target to $ip"

# If the entry does not exist, add it
if ! grep -q "target_robot.internal" /etc/hosts; then
    echo "$ip target_robot.internal" | sudo tee -a /etc/hosts
else
    # If the entry exists, replace it
    sudo sed -i "s/.*target_robot.internal/$ip target_robot.internal/g" /etc/hosts
fi
