#!/bin/bash

# Create a ROS bridge to the robot
zenoh-bridge-ros2dds --connect tcp/target_robot.internal:7447
