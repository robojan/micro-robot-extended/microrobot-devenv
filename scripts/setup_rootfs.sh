#!/bin/bash

ROOTFS_BASE=/rootfs
ROOTFS_DIR=$ROOTFS_BASE/microrobot/rootfs
SCRIPTS_DIR=$(realpath $(dirname $BASH_SOURCE))

source /opt/ros/$ROS_DISTRO/setup.bash

export CMAKE_TOOLCHAIN_FILE=$ROOTFS_DIR/toolchain.cmake
export COLCON_DEFAULTS_FILE=$SCRIPTS_DIR/microrobot_colcon_defaults.yaml
