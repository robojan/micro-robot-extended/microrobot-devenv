from rclpy.node import Node, ParameterDescriptor, ParameterType
from rclpy.qos import QoSPresetProfiles
import rclpy
from threading import Thread
from collections import deque
from tf2_ros.transform_listener import TransformListener
from tf2_ros.static_transform_broadcaster import StaticTransformBroadcaster
from tf2_ros.transform_broadcaster import TransformBroadcaster
from tf2_ros.buffer import Buffer
from geometry_msgs.msg import TransformStamped, Quaternion, PoseWithCovarianceStamped
import cv2
import cv2.aruco as aruco
import numpy as np
import time
import math


def quaternion_from_euler(ai, aj, ak):
    ai /= 2.0
    aj /= 2.0
    ak /= 2.0
    ci = math.cos(ai)
    si = math.sin(ai)
    cj = math.cos(aj)
    sj = math.sin(aj)
    ck = math.cos(ak)
    sk = math.sin(ak)
    cc = ci * ck
    cs = ci * sk
    sc = si * ck
    ss = si * sk

    q = np.empty((4,))
    q[0] = cj * sc - sj * cs
    q[1] = cj * ss + sj * cc
    q[2] = cj * cs - sj * sc
    q[3] = cj * cc + sj * ss

    return Quaternion(x=q[0], y=q[1], z=q[2], w=q[3])


def euler_from_quaternion(q):
    q0 = q.w
    q1 = q.x
    q2 = q.y
    q3 = q.z

    t0 = 2.0 * (q0 * q1 + q2 * q3)
    t1 = 1.0 - 2.0 * (q1 * q1 + q2 * q2)
    X = math.atan2(t0, t1)

    t2 = 2.0 * (q0 * q2 - q3 * q1)
    t2 = 1 if t2 > 1 else t2
    t2 = -1 if t2 < -1 else t2
    Y = math.asin(t2)

    t3 = 2.0 * (q0 * q3 + q1 * q2)
    t4 = 1.0 - 2.0 * (q2 * q2 + q3 * q3)
    Z = math.atan2(t3, t4)

    return X, Y, Z


class MarkerLocalizationNode(Node):
    def __init__(self, *args, **kwargs):
        super().__init__("marker_localization", *args, **kwargs)

        self.initialize_parameters()
        self.initialize_publishers()

        self._static_tf_broadcaster = StaticTransformBroadcaster(self)
        self._tf_broadcaster = TransformBroadcaster(self)
        self._tf_buffer = Buffer()
        self._tf_listener = TransformListener(self._tf_buffer, self)

        self._running = True
        self._thread = Thread(target=self._thread_task)
        self._thread.start()

        self._logger = self.get_logger()

        self._logger.info("Marker localization node initialized")

    def initialize_parameters(self):
        descriptor = ParameterDescriptor(
            name="camera_device",
            type=ParameterType.PARAMETER_STRING,
            description="Device of the camera",
            read_only=True,
        )
        self.declare_parameter("camera_device", "/dev/video0", descriptor)

        descriptor = ParameterDescriptor(
            name="world_frame",
            type=ParameterType.PARAMETER_STRING,
            description="Frame of the world",
            read_only=True,
        )
        self.declare_parameter("world_frame", "world", descriptor)

        descriptor = ParameterDescriptor(
            name="reference_marker1_description",
            type=ParameterType.PARAMETER_STRING,
            description="Description of the reference marker (<id>:<size>)",
            read_only=True,
        )
        self.declare_parameter("reference_marker1_description", "0:30", descriptor)

        descriptor = ParameterDescriptor(
            name="reference_marker1_frame",
            type=ParameterType.PARAMETER_STRING,
            description="Frame of the reference marker",
            read_only=True,
        )
        self.declare_parameter("reference_marker1_frame", "world_ref1", descriptor)

        descriptor = ParameterDescriptor(
            name="reference_marker1_pose",
            type=ParameterType.PARAMETER_DOUBLE_ARRAY,
            description="Pose of the reference marker (x y z r p y)",
            read_only=True,
        )
        self.declare_parameter(
            "reference_marker1_pose", [0.0, 0.0, 0.0, 0.0, 0.0, 0.0], descriptor
        )

        descriptor = ParameterDescriptor(
            name="reference_marker2_description",
            type=ParameterType.PARAMETER_STRING,
            description="Description of the reference marker (<id>:<size>)",
            read_only=True,
        )
        self.declare_parameter("reference_marker2_description", "1:30", descriptor)

        descriptor = ParameterDescriptor(
            name="reference_marker2_frame",
            type=ParameterType.PARAMETER_STRING,
            description="Frame of the reference marker",
            read_only=True,
        )
        self.declare_parameter(
            "reference_marker2_frame", "reference_marker", descriptor
        )

        descriptor = ParameterDescriptor(
            name="reference_marker2_pose",
            type=ParameterType.PARAMETER_DOUBLE_ARRAY,
            description="Pose of the reference marker (x y z r p y)",
            read_only=True,
        )
        self.declare_parameter(
            "reference_marker2_pose", [0.0, 0.0, 0.0, 0.0, 0.0, 0.0], descriptor
        )

        descriptor = ParameterDescriptor(
            name="marker1_description",
            type=ParameterType.PARAMETER_STRING,
            description="Description of the marker (<id>:<size>)",
            read_only=True,
        )
        self.declare_parameter("marker1_description", "2:30", descriptor)

        descriptor = ParameterDescriptor(
            name="marker1_frame",
            type=ParameterType.PARAMETER_STRING,
            description="Frame of the marker",
            read_only=True,
        )
        self.declare_parameter("marker1_frame", "marker1", descriptor)

        descriptor = ParameterDescriptor(
            name="marker1_base_frame",
            type=ParameterType.PARAMETER_STRING,
            description="Base frame of the marker",
            read_only=True,
        )
        self.declare_parameter("marker1_base_frame", "", descriptor)

        descriptor = ParameterDescriptor(
            name="debug",
            type=ParameterType.PARAMETER_BOOL,
            description="Debug mode",
            read_only=True,
        )
        self.declare_parameter("debug", False, descriptor)
        self._debug = self.get_parameter("debug").get_parameter_value().bool_value

    def initialize_publishers(self):
        # Marker 1 publisher
        self._marker1_publisher = self.create_publisher(
            PoseWithCovarianceStamped,
            "marker1_pose",
            QoSPresetProfiles.SENSOR_DATA.value,
        )

    def _thread_task(self):
        ref_points1 = None
        ref_points2 = None
        while self._running:
            try:
                # Try to open the camera
                camera_device = (
                    self.get_parameter("camera_device")
                    .get_parameter_value()
                    .string_value
                )
                cap = cv2.VideoCapture(camera_device)
                if not cap.isOpened():
                    raise Exception("Error opening camera")

                # Load the parameters
                def load_ref_marker(name):
                    id = int(
                        self.get_parameter(name + "_description")
                        .get_parameter_value()
                        .string_value.split(":")[0]
                    )
                    size = (
                        float(
                            self.get_parameter(name + "_description")
                            .get_parameter_value()
                            .string_value.split(":")[1]
                        )
                        / 1000  # Convert to meters
                    )
                    frame = (
                        self.get_parameter(name + "_frame")
                        .get_parameter_value()
                        .string_value
                    )
                    pose = np.array(
                        [
                            float(x)
                            for x in self.get_parameter(name + "_pose")
                            .get_parameter_value()
                            .double_array_value
                        ]
                    )
                    # Only work in 2D
                    p = pose[0:2]
                    rotation = np.array(
                        [
                            [np.cos(pose[5]), -np.sin(pose[5])],
                            [np.sin(pose[5]), np.cos(pose[5])],
                        ]
                    )
                    back = np.dot(rotation, (-size, 0))
                    right = np.dot(rotation, (0, -size))
                    points = np.array(
                        (
                            (p[0], p[1]),
                            (p[0], p[1]) + right,
                            (p[0], p[1]) + back + right,
                            (p[0], p[1]) + back,
                        )
                    )
                    return {
                        "id": id,
                        "size": size,
                        "frame": frame,
                        "points": points,
                        "pose": pose,
                    }

                ref_marker1 = load_ref_marker("reference_marker1")
                ref_marker2 = load_ref_marker("reference_marker2")

                HISTORY_SIZE = 100

                marker1 = {
                    "id": int(
                        self.get_parameter("marker1_description")
                        .get_parameter_value()
                        .string_value.split(":")[0]
                    ),
                    "size": float(
                        self.get_parameter("marker1_description")
                        .get_parameter_value()
                        .string_value.split(":")[1]
                    ),
                    "frame": self.get_parameter("marker1_frame")
                    .get_parameter_value()
                    .string_value,
                    "base_frame": self.get_parameter("marker1_base_frame")
                    .get_parameter_value()
                    .string_value,
                    "history": deque(maxlen=HISTORY_SIZE),
                }

                world_frame = (
                    self.get_parameter("world_frame").get_parameter_value().string_value
                )

                # Publish the static reference frames
                def get_static_transform(frame_id, child_frame_id, pose):
                    transform = TransformStamped()
                    transform.transform.translation.x = pose[0]
                    transform.transform.translation.y = pose[1]
                    transform.transform.translation.z = pose[2]
                    transform.transform.rotation = quaternion_from_euler(*pose[3:])
                    transform.header.frame_id = frame_id
                    transform.child_frame_id = child_frame_id
                    transform.header.stamp = self.get_clock().now().to_msg()
                    return transform

                self._static_tf_broadcaster.sendTransform(
                    [
                        get_static_transform(
                            world_frame, ref_marker1["frame"], ref_marker1["pose"]
                        ),
                        get_static_transform(
                            world_frame, ref_marker2["frame"], ref_marker2["pose"]
                        ),
                    ]
                )

                # Create the aruco dictionary
                aruco_dict = aruco.getPredefinedDictionary(aruco.DICT_4X4_50)

                # Create the aruco detector
                aruco_params = aruco.DetectorParameters()

                refine_params = aruco.RefineParameters()

                detector = aruco.ArucoDetector(aruco_dict, aruco_params, refine_params)

                while self._running:
                    # Capture frame-by-frame
                    ret, frame = cap.read()
                    if not ret:
                        raise Exception("Error reading frame")
                    frame_timestamp = self.get_clock().now()

                    # Convert to grayscale
                    gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)

                    # Detect markers
                    markerCorners, markerIds, rejectedCandidates = (
                        detector.detectMarkers(gray)
                    )

                    # Find relevant markers
                    def find_marker(id):
                        if markerIds is None:
                            return None

                        for i in range(len(markerIds)):
                            if markerIds[i][0] == id:
                                return markerCorners[i][0]
                        return None

                    ref1_corner = find_marker(ref_marker1["id"])
                    ref2_corner = find_marker(ref_marker2["id"])
                    marker_corner = find_marker(marker1["id"])

                    # Update reference points
                    def update_ref_points(ref_points, ref_corner):
                        if ref_corner is not None:
                            ref_points = (
                                ref_points * 0.9 + ref_corner * 0.1
                                if not ref_points is None
                                else ref_corner
                            )
                        return ref_points

                    ref_points1 = update_ref_points(ref_points1, ref1_corner)
                    ref_points2 = update_ref_points(ref_points2, ref2_corner)

                    # Calculate the pose of the marker
                    if (
                        ref_points1 is not None
                        and ref_points2 is not None
                        and marker_corner is not None
                    ):
                        # Calculate the conversion matrix
                        screen_points = np.hstack(
                            (np.vstack((ref_points1, ref_points2)), np.ones((8, 1)))
                        )
                        world_points = np.vstack(
                            (ref_marker1["points"], ref_marker2["points"])
                        )
                        conversion_matrix, residuals, rank, s = np.linalg.lstsq(
                            screen_points, world_points, rcond=None
                        )

                        # Calculate the position of the marker
                        marker_points = (
                            np.hstack((marker_corner, np.ones((4, 1))))
                            @ conversion_matrix
                        )
                        marker_position = marker_points[0]
                        marker_direction = marker_points[0] - marker_points[3]
                        marker_rotation = np.arctan2(
                            marker_direction[1], marker_direction[0]
                        )
                        marker_pose = np.array(
                            [
                                marker_position[0],
                                marker_position[1],
                                ref_marker1["pose"][2],
                                0,
                                0,
                                marker_rotation,
                            ]
                        )

                        # Calculate the covariance of the marker
                        marker1["history"].append(marker_pose)

                        marker_mean = np.mean(marker1["history"], axis=0)
                        marker_deviations = marker1["history"] - marker_mean
                        if len(marker1["history"]) < 50:
                            marker_covariance = np.eye(6) * 0.01
                        else:
                            marker_covariance = np.cov(marker_deviations, rowvar=False)

                        # Publish the Pose
                        msg = PoseWithCovarianceStamped()
                        msg.header.frame_id = world_frame
                        msg.header.stamp = frame_timestamp.to_msg()
                        msg.pose.pose.position.x = marker_pose[0]
                        msg.pose.pose.position.y = marker_pose[1]
                        msg.pose.pose.position.z = marker_pose[2]
                        msg.pose.pose.orientation = quaternion_from_euler(
                            0, 0, marker_pose[5]
                        )
                        msg.pose.covariance = marker_covariance.flatten()
                        self._marker1_publisher.publish(msg)

                        # Get the odom frame
                        if not marker1["base_frame"] == "":
                            try:
                                odom_frame = self._tf_buffer.lookup_transform(
                                    marker1["frame"],
                                    marker1["base_frame"],
                                    rclpy.time.Time(),
                                )

                                # Publish the marker transform
                                transform = TransformStamped()
                                transform.header.frame_id = world_frame
                                transform.child_frame_id = marker1["frame"]
                                transform.header.stamp = frame_timestamp.to_msg()
                                transform.transform.translation.x = (
                                    marker_pose[0] - odom_frame.transform.translation.x
                                )
                                transform.transform.translation.y = (
                                    marker_pose[1] - odom_frame.transform.translation.y
                                )
                                transform.transform.translation.z = (
                                    marker_pose[2] - odom_frame.transform.translation.z
                                )
                                odom_rotation = euler_from_quaternion(
                                    odom_frame.transform.rotation
                                )
                                transform.transform.rotation = quaternion_from_euler(
                                    marker_pose[3] - odom_rotation[0],
                                    marker_pose[4] - odom_rotation[1],
                                    marker_pose[5] - odom_rotation[2],
                                )
                                self._tf_broadcaster.sendTransform(transform)
                            except Exception as e:
                                self._logger.error(str(e))

                    if self._debug:
                        # Draw detected markers
                        frame = aruco.drawDetectedMarkers(
                            frame, markerCorners, markerIds
                        )

                        # Draw reference points
                        if not ref_points1 is None:
                            pos = (int(ref_points1[0][0]), int(ref_points1[0][1]))
                            cv2.circle(frame, pos, 5, (0, 255, 255), -1)
                            pos = (int(ref_points1[1][0]), int(ref_points1[1][1]))
                            cv2.circle(frame, pos, 5, (0, 255, 0), -1)
                            pos = (int(ref_points1[2][0]), int(ref_points1[2][1]))
                            cv2.circle(frame, pos, 5, (0, 255, 96), -1)
                            pos = (int(ref_points1[3][0]), int(ref_points1[3][1]))
                            cv2.circle(frame, pos, 5, (0, 255, 192), -1)
                        if not ref_points2 is None:
                            pos = (int(ref_points2[0][0]), int(ref_points2[0][1]))
                            cv2.circle(frame, pos, 5, (0, 255, 255), -1)
                            pos = (int(ref_points2[1][0]), int(ref_points2[1][1]))
                            cv2.circle(frame, pos, 5, (0, 0, 255), -1)
                            pos = (int(ref_points2[2][0]), int(ref_points2[2][1]))
                            cv2.circle(frame, pos, 5, (0, 96, 255), -1)
                            pos = (int(ref_points2[3][0]), int(ref_points2[3][1]))
                            cv2.circle(frame, pos, 5, (0, 192, 255), -1)

                        # Display the resulting frame
                        cv2.imshow("frame", frame)

                        # Update the window
                        cv2.waitKey(1)

            except Exception as e:
                self._logger.error(str(e))
                if self._running:
                    time.sleep(5)

            cv2.destroyAllWindows()
            cap.release()

    def stop(self):
        self._running = False
        self._thread.join()
        self._logger.info("Marker localization node stopped")
