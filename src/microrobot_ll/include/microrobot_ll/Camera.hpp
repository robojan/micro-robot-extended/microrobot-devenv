#pragma once

#include <rclcpp/rclcpp.hpp>
#include <sensor_msgs/msg/image.hpp>
#include <sensor_msgs/msg/camera_info.hpp>
#include <thread>
#include <linux/videodev2.h>
#include <linux/v4l2-subdev.h>
#include <cstdint>
#include <array>
#include <span>
#include <cstddef>

class Camera
{
  public:
    Camera(rclcpp::Node& node);
    virtual ~Camera() = default;

  private:
    static constexpr auto kCameraFormat = V4L2_PIX_FMT_UYVY;
    static constexpr auto kCameraSubdevFormat = V4L2_MBUS_FMT_SGBRG10_1X10;
    static constexpr auto kNumBuffers = 4;
    static constexpr auto kCaptureDevice = "/dev/video0";
    static constexpr auto kCameraSubdevice = "/dev/v4l-subdev3";
    static constexpr auto kIspParamsDevice = "/dev/video8";
    static constexpr auto kIspStatsDevice = "/dev/video7";
    static constexpr auto kCameraSourceWidth = 2304;
    static constexpr auto kCameraSourceHeight = 1296;

    struct Parameters
    {
        int width = 2304;
        int height = 1296;
        std::string frame_id = "camera_link_optical";
    };

    rclcpp::Node& _node;
    Parameters _parameters;
    int _capture_fd = -1;
    int _camera_fd = -1;
    std::jthread _capture_thread;
    std::array<std::span<std::byte>, kNumBuffers> _buffers;

    rclcpp::Publisher<sensor_msgs::msg::Image>::SharedPtr _image_pub;
    rclcpp::Publisher<sensor_msgs::msg::CameraInfo>::SharedPtr _camera_info_pub;

    void initialize_parameters();
    void initialize_publications();
    void initialize_device();
    void deinitialize_device();
    void capture_thread(std::stop_token stop_token);

    void publish_image(const std::span<std::byte> buffer);
};
