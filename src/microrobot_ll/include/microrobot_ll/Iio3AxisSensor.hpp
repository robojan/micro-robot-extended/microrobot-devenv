#pragma once

#include <rclcpp/rclcpp.hpp>
#include <iiopp.h>
#include <memory>
#include <array>
#include <unordered_map>
#include <functional>

class Iio3AxisSensor
{
public:
    struct DataSample {
        std::array<double, 3> value {nan(""), nan(""), nan("")};
        int64_t timestamp {0};
    };

    Iio3AxisSensor(rclcpp::Node &node, iiopp::Context &context, const std::string &device_name, std::unordered_map<std::string, std::string> &parameters, const std::function<void(const DataSample &)> &callback);
    Iio3AxisSensor(const Iio3AxisSensor &) = delete;
    Iio3AxisSensor &operator=(const Iio3AxisSensor &) = delete;
    ~Iio3AxisSensor() = default;

private:
    rclcpp::Node &_node;
    iiopp::Context &_context;
    std::string _device_name;
    std::unordered_map<std::string, std::string> _parameters;
    std::jthread _capture_thread;
    std::function<void(const DataSample &)> _callback;

    void capture_thread(std::stop_token stop_token);
};
