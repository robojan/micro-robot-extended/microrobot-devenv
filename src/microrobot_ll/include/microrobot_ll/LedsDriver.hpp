#pragma once

#include <rclcpp/rclcpp.hpp>
#include <microrobot_interfaces/msg/leds.hpp>
#include <microrobot_interfaces/srv/set_led_mode.hpp>

class LedsDriver
{
  public:
    LedsDriver(rclcpp::Node& node);
    virtual ~LedsDriver() = default;

    void setManualMode();
    void setAutomaticMode();

    void setLedColor(int led, uint8_t r, uint8_t g, uint8_t b);

  private:
    struct Parameters
    {
        std::array<std::string, 4> deviceR{ "/sys/class/leds/red:indicator-0", "/sys/class/leds/red:indicator-1",
                                            "/sys/class/leds/red:indicator-2", "/sys/class/leds/red:indicator-3" };
        std::array<std::string, 4> deviceG{ "/sys/class/leds/green:indicator-0", "/sys/class/leds/green:indicator-1",
                                            "/sys/class/leds/green:indicator-2", "/sys/class/leds/green:indicator-3" };
        std::array<std::string, 4> deviceB{ "/sys/class/leds/blue:indicator-0", "/sys/class/leds/blue:indicator-1",
                                            "/sys/class/leds/blue:indicator-2", "/sys/class/leds/blue:indicator-3" };
    };

    rclcpp::Node& _node;
    Parameters _parameters;

    rclcpp::Subscription<microrobot_interfaces::msg::Leds>::SharedPtr _leds_sub;
    rclcpp::Service<microrobot_interfaces::srv::SetLedMode>::SharedPtr _set_led_mode_srv;

    void initialize_parameters();
    void initialize_subscriptions();
    void initialize_services();

    void leds_callback(const microrobot_interfaces::msg::Leds::SharedPtr msg);
    void set_led_mode_callback(const std::shared_ptr<rmw_request_id_t> request_header,
                               const std::shared_ptr<microrobot_interfaces::srv::SetLedMode::Request> request,
                               const std::shared_ptr<microrobot_interfaces::srv::SetLedMode::Response> response);
};
