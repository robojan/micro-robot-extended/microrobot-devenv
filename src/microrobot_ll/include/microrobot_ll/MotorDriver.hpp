#pragma once

#include <rclcpp/rclcpp.hpp>
#include <geometry_msgs/msg/twist.hpp>

class MotorDriver
{
public:
    MotorDriver(rclcpp::Node &node);
    virtual ~MotorDriver() = default;

    void set_motor_speeds(float m1, float m2, float m3, float m4);

private:
    struct Parameters {
        std::string device = "/dev/motors";
    };

    rclcpp::Node &_node;
    Parameters _parameters;
    int _fd;

    rclcpp::Subscription<geometry_msgs::msg::Twist>::SharedPtr _cmd_vel_sub;

    void initialize_parameters();
    void initialize_subscriptions();
    void initialize_device();

    void cmd_vel_callback(const geometry_msgs::msg::Twist::SharedPtr msg);
};
