#include <microrobot_ll/Camera.hpp>

#include <cstdio>
#include <cstring>
#include <fcntl.h>
#include <sys/ioctl.h>
#include <sys/mman.h>
#include <unistd.h>
#include <linux/v4l2-subdev.h>

#include <rclcpp/logging.hpp>
#include <sensor_msgs/image_encodings.hpp>
#include <rclcpp/qos.hpp>

#include <chrono>

Camera::Camera(rclcpp::Node& node) : _node(node)
{
    initialize_parameters();
    initialize_publications();
    initialize_device();

    _capture_thread = std::jthread(&Camera::capture_thread, this);
}

void Camera::initialize_parameters()
{
    rcl_interfaces::msg::ParameterDescriptor device_descriptor;

    device_descriptor.name = "width";
    device_descriptor.type = rcl_interfaces::msg::ParameterType::PARAMETER_INTEGER;
    device_descriptor.description = "Width of the camera image";
    _node.declare_parameter("width", _parameters.width);
    _parameters.width = _node.get_parameter("width").as_int();

    device_descriptor.name = "height";
    device_descriptor.type = rcl_interfaces::msg::ParameterType::PARAMETER_INTEGER;
    device_descriptor.description = "Height of the camera image";
    _node.declare_parameter("height", _parameters.height);
    _parameters.height = _node.get_parameter("height").as_int();

    device_descriptor.name = "camera_frame";
    device_descriptor.type = rcl_interfaces::msg::ParameterType::PARAMETER_STRING;
    device_descriptor.description = "Frame ID for the camera";
    _node.declare_parameter("camera_frame", _parameters.frame_id);
    _parameters.frame_id = _node.get_parameter("camera_frame").as_string();
}

void Camera::initialize_publications()
{
    _image_pub = _node.create_publisher<sensor_msgs::msg::Image>("image_raw", rclcpp::SensorDataQoS{});
    _camera_info_pub = _node.create_publisher<sensor_msgs::msg::CameraInfo>("camera_info", rclcpp::SensorDataQoS{});
}

void Camera::initialize_device()
{
    _capture_fd = open(kCaptureDevice, O_RDWR);
    if (_capture_fd < 0)
    {
        RCLCPP_ERROR(_node.get_logger(), "Failed to open camera %s: %s", kCaptureDevice, strerror(errno));
        throw std::runtime_error("Failed to open camera");
    }

    _camera_fd = open(kCameraSubdevice, O_RDWR);
    if (_camera_fd < 0)
    {
        RCLCPP_ERROR(_node.get_logger(), "Failed to open camera %s: %s", kCameraSubdevice, strerror(errno));
        throw std::runtime_error("Failed to open camera sub device");
    }

    struct v4l2_capability cap;
    if (ioctl(_capture_fd, VIDIOC_QUERYCAP, &cap) < 0)
    {
        RCLCPP_ERROR(_node.get_logger(), "Failed to query camera capabilities: %s", strerror(errno));
        throw std::runtime_error("Failed to query camera capabilities");
    }

    if (!(cap.capabilities & V4L2_CAP_VIDEO_CAPTURE_MPLANE))
    {
        RCLCPP_ERROR(_node.get_logger(), "Camera does not support multiplanar video capture");
        throw std::runtime_error("Camera does not support multiplanar video capture");
    }

    if (!(cap.capabilities & V4L2_CAP_STREAMING))
    {
        RCLCPP_ERROR(_node.get_logger(), "Camera does not support streaming");
        throw std::runtime_error("Camera does not support streaming");
    }

    struct v4l2_subdev_format subdev_format = {};
    subdev_format.pad = 0;
    subdev_format.which = V4L2_SUBDEV_FORMAT_ACTIVE;
    if (ioctl(_camera_fd, VIDIOC_SUBDEV_G_FMT, &subdev_format) < 0)
    {
        RCLCPP_ERROR(_node.get_logger(), "Failed to get camera subdevice format: %s", strerror(errno));
        throw std::runtime_error("Failed to get camera subdevice format");
    }

    subdev_format.format.width = kCameraSourceWidth;
    subdev_format.format.height = kCameraSourceHeight;
    subdev_format.format.code = kCameraSubdevFormat;
    subdev_format.format.field = V4L2_FIELD_NONE;
    if (ioctl(_camera_fd, VIDIOC_SUBDEV_S_FMT, &subdev_format) < 0)
    {
        RCLCPP_ERROR(_node.get_logger(), "Failed to set camera subdevice format: %s", strerror(errno));
        throw std::runtime_error("Failed to set camera subdevice format");
    }

    struct v4l2_format format = {};
    format.type = V4L2_BUF_TYPE_VIDEO_CAPTURE_MPLANE;
    format.fmt.pix_mp.width = _parameters.width;
    format.fmt.pix_mp.height = _parameters.height;
    format.fmt.pix_mp.pixelformat = kCameraFormat;
    format.fmt.pix_mp.field = V4L2_FIELD_NONE;
    if (ioctl(_capture_fd, VIDIOC_S_FMT, &format) < 0)
    {
        RCLCPP_ERROR(_node.get_logger(), "Failed to set camera format: %s", strerror(errno));
        throw std::runtime_error("Failed to set camera format");
    }

    // Flip vertically
    struct v4l2_control control = {};
    control.id = V4L2_CID_VFLIP;
    control.value = 1;
    if (ioctl(_capture_fd, VIDIOC_S_CTRL, &control) < 0)
    {
        RCLCPP_WARN(_node.get_logger(), "Failed to set camera vertical flip: %s", strerror(errno));
    }

    struct v4l2_requestbuffers req = {};
    req.count = kNumBuffers;
    req.type = V4L2_BUF_TYPE_VIDEO_CAPTURE_MPLANE;
    req.memory = V4L2_MEMORY_MMAP;
    if (ioctl(_capture_fd, VIDIOC_REQBUFS, &req) < 0)
    {
        RCLCPP_ERROR(_node.get_logger(), "Failed to request camera buffers: %s", strerror(errno));
        throw std::runtime_error("Failed to request camera buffers");
    }

    for (int i = 0; i < kNumBuffers; ++i)
    {
        struct v4l2_plane planes[1] = {};
        planes[0].length = format.fmt.pix_mp.plane_fmt[0].sizeimage;
        planes[0].m.mem_offset = format.fmt.pix_mp.plane_fmt[0].reserved[0];

        struct v4l2_buffer buf = {};
        buf.type = V4L2_BUF_TYPE_VIDEO_CAPTURE_MPLANE;
        buf.memory = V4L2_MEMORY_MMAP;
        buf.index = i;
        buf.m.planes = planes;
        buf.length = 1;
        if (ioctl(_capture_fd, VIDIOC_QUERYBUF, &buf) < 0)
        {
            RCLCPP_ERROR(_node.get_logger(), "Failed to query camera buffer: %s", strerror(errno));
            throw std::runtime_error("Failed to query camera buffer");
        }

        std::byte* ptr = (std::byte*)mmap(NULL, buf.m.planes[0].length, PROT_READ | PROT_WRITE, MAP_SHARED, _capture_fd,
                                          buf.m.planes[0].m.mem_offset);
        if (ptr == MAP_FAILED)
        {
            RCLCPP_ERROR(_node.get_logger(), "Failed to mmap camera buffer: %s", strerror(errno));
            throw std::runtime_error("Failed to mmap camera buffer");
        }

        _buffers[i] = std::span<std::byte>(ptr, buf.m.planes[0].length);

        if (ioctl(_capture_fd, VIDIOC_QBUF, &buf) < 0)
        {
            RCLCPP_ERROR(_node.get_logger(), "Failed to queue camera buffer: %s", strerror(errno));
            throw std::runtime_error("Failed to queue camera buffer");
        }
    }
}

void Camera::deinitialize_device()
{
    for (int i = 0; i < kNumBuffers; ++i)
    {
        munmap(_buffers[i].data(), _buffers[i].size());
    }

    close(_capture_fd);
    close(_camera_fd);
}

void Camera::capture_thread(std::stop_token stop_token)
{
    // Start streaming
    static constexpr auto buffer_type = V4L2_BUF_TYPE_VIDEO_CAPTURE_MPLANE;
    if (ioctl(_capture_fd, VIDIOC_STREAMON, &buffer_type) < 0)
    {
        RCLCPP_ERROR(_node.get_logger(), "Failed to start camera streaming: %s", strerror(errno));
        throw std::runtime_error("Failed to start camera streaming");
    }

    std::vector<std::byte> testBuffer;
    testBuffer.resize(_buffers[0].size());

    auto clock = std::chrono::steady_clock();
    auto last_frame_time = clock.now();
    while (!stop_token.stop_requested())
    {
        // Wait for a frame
        struct v4l2_plane planes[1] = {};
        struct v4l2_buffer buf = {};
        buf.type = V4L2_BUF_TYPE_VIDEO_CAPTURE_MPLANE;
        buf.memory = V4L2_MEMORY_MMAP;
        buf.m.planes = planes;
        buf.length = 1;
        if (ioctl(_capture_fd, VIDIOC_DQBUF, &buf) < 0)
        {
            RCLCPP_ERROR(_node.get_logger(), "Failed to dequeue camera buffer: %s", strerror(errno));
            throw std::runtime_error("Failed to dequeue camera buffer");
        }
        auto frame_time = clock.now();

        // Publish the frame
        publish_image(_buffers[buf.index]);
        auto publish_time = clock.now();

        // Release the frame
        if (ioctl(_capture_fd, VIDIOC_QBUF, &buf) < 0)
        {
            RCLCPP_ERROR(_node.get_logger(), "Failed to queue camera buffer: %s", strerror(errno));
            throw std::runtime_error("Failed to queue camera buffer");
        }
        auto release_time = clock.now();

        // Calculate timing statistics
        auto frame_duration = std::chrono::duration_cast<std::chrono::microseconds>(frame_time - last_frame_time);
        auto publish_duration = std::chrono::duration_cast<std::chrono::microseconds>(publish_time - frame_time);
        auto release_duration = std::chrono::duration_cast<std::chrono::microseconds>(release_time - publish_time);
        last_frame_time = frame_time;

        RCLCPP_DEBUG(_node.get_logger(), "Frame duration: %ld us, publish duration: %ld us, release duration: %ld us",
                     frame_duration.count(), publish_duration.count(), release_duration.count());
    }

    // Stop streaming
    if (ioctl(_capture_fd, VIDIOC_STREAMOFF, &buffer_type) < 0)
    {
        RCLCPP_ERROR(_node.get_logger(), "Failed to stop camera streaming: %s", strerror(errno));
        throw std::runtime_error("Failed to stop camera streaming");
    }

    deinitialize_device();
}

void Camera::publish_image(const std::span<std::byte> buffer)
{
    auto msg = sensor_msgs::msg::Image();
    msg.header.stamp = _node.now();
    msg.header.frame_id = _parameters.frame_id;
    msg.height = _parameters.height;
    msg.width = _parameters.width;
    msg.encoding = sensor_msgs::image_encodings::YUV422;
    msg.step = _parameters.width * 2;
    uint8_t* data_begin = reinterpret_cast<uint8_t*>(buffer.data());
    msg.data = std::vector<uint8_t>(data_begin, data_begin + buffer.size());
    _image_pub->publish(msg);

    auto camera_info = sensor_msgs::msg::CameraInfo();
    camera_info.header.stamp = _node.now();
    camera_info.header.frame_id = _parameters.frame_id;
    camera_info.height = _parameters.height;
    camera_info.width = _parameters.width;
    camera_info.distortion_model = "plumb_bob";
    camera_info.d = { 0.0, 0.0, 0.0, 0.0, 0.0 };
    camera_info.k = { 1.0, 0.0, 0.0, 0.0, 1.0, 0.0, 0.0, 0.0, 1.0 };
    camera_info.r = { 1.0, 0.0, 0.0, 0.0, 1.0, 0.0, 0.0, 0.0, 1.0 };
    camera_info.p = { 1.0, 0.0, 0.0, 0.0, 0.0, 1.0, _parameters.width / 2.0, _parameters.height / 2.0,
                      0.0, 0.0, 1.0, 0.0 };
    _camera_info_pub->publish(camera_info);
}
