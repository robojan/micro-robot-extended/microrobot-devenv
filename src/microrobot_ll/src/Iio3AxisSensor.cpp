#include <microrobot_ll/Iio3AxisSensor.hpp>

union IioData {
    int64_t s64;
    int32_t s32;
    int16_t s16;
    int8_t s8;
    uint64_t u64;
    uint32_t u32;
    uint16_t u16;
    uint8_t u8;
};

double dataToDouble(const iio_data_format &format, const IioData &data)
{
    double raw;
    switch (format.length) {
    case 64:
        raw = format.is_signed ? data.s64 : data.u64;
        break;
    case 32:
        raw = format.is_signed ? data.s32 : data.u32;
        break;
    case 16:
        raw = format.is_signed ? data.s16 : data.u16;
        break;
    case 8:
        raw = format.is_signed ? data.s8 : data.u8;
        break;
    default:
        return nan("");
    }

    if(format.with_scale) {
        return (raw + format.offset) * format.scale;
    } else {
        return raw;
    }
}

long long dataToInteger(const iio_data_format &format, const IioData &data)
{
    long long raw;
    switch (format.length) {
    case 64:
        raw = format.is_signed ? data.s64 : data.u64;
        break;
    case 32:
        raw = format.is_signed ? data.s32 : data.u32;
        break;
    case 16:
        raw = format.is_signed ? data.s16 : data.u16;
        break;
    case 8:
        raw = format.is_signed ? data.s8 : data.u8;
        break;
    default:
        return 0;
    }

    if(format.with_scale) {
        return (raw + format.offset) * format.scale;
    } else {
        return raw;
    }
}

Iio3AxisSensor::Iio3AxisSensor(rclcpp::Node &node, iiopp::Context &context, const std::string &device_name, std::unordered_map<std::string, std::string> &parameters, const std::function<void(const DataSample &)> &callback) : 
    _node(node), _context{context}, _device_name{device_name}, _parameters{parameters}, _callback{callback}
{
    assert(_callback);
    assert(!_device_name.empty());
    _capture_thread = std::jthread(&Iio3AxisSensor::capture_thread, this);
}

void Iio3AxisSensor::capture_thread(std::stop_token stop_token)
{
    try {
        static constexpr int kNumBlock = 4;
        static constexpr int kNumSamplesPerBlock = 1;

        // Find the device and its channels
        auto dev = _context.find_device(_device_name.c_str());
        if (!dev) {
            RCLCPP_ERROR(_node.get_logger(), "Failed to find gyro device");
            return;
        }

        // Configure the device
        for(auto &param : _parameters) {
            try {
                auto attr = dev->find_attr(param.first.c_str());
                if (!attr) {
                    throw std::runtime_error("Failed to find attribute");
                }
                attr->write_string(param.second.c_str());
            } catch (std::exception &e) {
                RCLCPP_ERROR(_node.get_logger(), "Failed to set parameter %s for %s: %s", param.first.c_str(), _device_name.c_str(), e.what());
            }
        }

        // Create a mask for the channels we want to enable
        auto chan_mask = std::unique_ptr<iio_channels_mask, void (*)(iio_channels_mask *)>(iio_create_channels_mask(dev->channels_count()), iio_channels_mask_destroy);

        // Enable the channels we want
        for (auto channel : dev.value()) {
            if(channel.modifier() == IIO_MOD_X || channel.modifier() == IIO_MOD_Y || channel.modifier() == IIO_MOD_Z || channel.type() == IIO_TIMESTAMP){
                if(channel.data_format()->repeat > 1) {
                    RCLCPP_ERROR(_node.get_logger(), "Data format repeat is not supported for channel %s", channel.id().c_str());
                    continue;
                }
                channel.enable(chan_mask.get());
            }
        }

        auto buffer = dev->create_buffer(0, chan_mask.get());
        if (!buffer) {
            RCLCPP_ERROR(_node.get_logger(), "Failed to create gyro buffer");
            return;
        }

        // Start the streaming
        auto stream = buffer->create_stream(kNumBlock, kNumSamplesPerBlock);
        if (!stream) {
            RCLCPP_ERROR(_node.get_logger(), "Failed to create gyro stream");
            return;
        }

        while (!stop_token.stop_requested()) {
            auto data_block = stream->next_block();

            DataSample sample;

            data_block.foreach_sample(chan_mask.get(), [](const iio_channel *channel, void *src, size_t bytes, void *user) -> ssize_t{
                auto &sample = *static_cast<DataSample *>(user);
                
                IioData convertedData;
                iio_channel_convert(channel, &convertedData, src);
                auto channel_type = iio_channel_get_type(channel);
                auto channel_modifier = iio_channel_get_modifier(channel);
                if(channel_type == IIO_TIMESTAMP) {
                    sample.timestamp = dataToInteger(*iio_channel_get_data_format(channel), convertedData);
                } else {
                    auto val = dataToDouble(*iio_channel_get_data_format(channel), convertedData);
                    if(channel_modifier == IIO_MOD_X) {
                        sample.value[0] = val;
                    } else if(channel_modifier == IIO_MOD_Y) {
                        sample.value[1] = val;
                    } else if(channel_modifier == IIO_MOD_Z) {
                        sample.value[2] = val;
                    }
                }
                return bytes;
            }, &sample);

            _callback(sample);
        }
    } catch (iiopp::error &e) {
        RCLCPP_ERROR(_node.get_logger(), "IIO error: %s", e.what());
    }
}
