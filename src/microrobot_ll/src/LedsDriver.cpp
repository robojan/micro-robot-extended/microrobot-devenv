#include <microrobot_ll/LedsDriver.hpp>

#include <fstream>

namespace
{
    void setLedTrigger(const std::string &device, const std::string &trigger)
    {
        std::ofstream file(device + "/trigger");
        file << trigger;
    }

    void setLedBrightness(const std::string &device, uint8_t brightness)
    {
        std::ofstream file(device + "/brightness");
        file << static_cast<int>(brightness);
    }

    void setLedPattern(const std::string &device, const std::string &pattern)
    {
        std::ofstream file(device + "/pattern");
        file << pattern;
    }
}

LedsDriver::LedsDriver(rclcpp::Node &node) : _node(node)
{
    initialize_parameters();
    initialize_subscriptions();
    initialize_services();
}

void LedsDriver::initialize_parameters()
{
    rcl_interfaces::msg::ParameterDescriptor device_descriptor;
    device_descriptor.type = rcl_interfaces::msg::ParameterType::PARAMETER_STRING;

    device_descriptor.name = "device_1_red";
    device_descriptor.description = "Path to the device file for the red led of the first indicator";
    _node.declare_parameter("device_1_red", _parameters.deviceR[0]);
    _parameters.deviceR[0] = _node.get_parameter("device_1_red").as_string();

    device_descriptor.name = "device_1_green";
    device_descriptor.description = "Path to the device file for the green led of the first indicator";
    _node.declare_parameter("device_1_green", _parameters.deviceG[0]);
    _parameters.deviceG[0] = _node.get_parameter("device_1_green").as_string();

    device_descriptor.name = "device_1_blue";
    device_descriptor.description = "Path to the device file for the blue led of the first indicator";
    _node.declare_parameter("device_1_blue", _parameters.deviceB[0]);
    _parameters.deviceB[0] = _node.get_parameter("device_1_blue").as_string();

    device_descriptor.name = "device_2_red";
    device_descriptor.description = "Path to the device file for the red led of the second indicator";
    _node.declare_parameter("device_2_red", _parameters.deviceR[1]);
    _parameters.deviceR[1] = _node.get_parameter("device_2_red").as_string();

    device_descriptor.name = "device_2_green";
    device_descriptor.description = "Path to the device file for the green led of the second indicator";
    _node.declare_parameter("device_2_green", _parameters.deviceG[1]);
    _parameters.deviceG[1] = _node.get_parameter("device_2_green").as_string();

    device_descriptor.name = "device_2_blue";
    device_descriptor.description = "Path to the device file for the blue led of the second indicator";
    _node.declare_parameter("device_2_blue", _parameters.deviceB[1]);
    _parameters.deviceB[1] = _node.get_parameter("device_2_blue").as_string();

    device_descriptor.name = "device_3_red";
    device_descriptor.description = "Path to the device file for the red led of the third indicator";
    _node.declare_parameter("device_3_red", _parameters.deviceR[2]);
    _parameters.deviceR[2] = _node.get_parameter("device_3_red").as_string();

    device_descriptor.name = "device_3_green";
    device_descriptor.description = "Path to the device file for the green led of the third indicator";
    _node.declare_parameter("device_3_green", _parameters.deviceG[2]);
    _parameters.deviceG[2] = _node.get_parameter("device_3_green").as_string();

    device_descriptor.name = "device_3_blue";
    device_descriptor.description = "Path to the device file for the blue led of the third indicator";
    _node.declare_parameter("device_3_blue", _parameters.deviceB[2]);
    _parameters.deviceB[2] = _node.get_parameter("device_3_blue").as_string();

    device_descriptor.name = "device_4_red";
    device_descriptor.description = "Path to the device file for the red led of the fourth indicator";
    _node.declare_parameter("device_4_red", _parameters.deviceR[3]);
    _parameters.deviceR[3] = _node.get_parameter("device_4_red").as_string();

    device_descriptor.name = "device_4_green";
    device_descriptor.description = "Path to the device file for the green led of the fourth indicator";
    _node.declare_parameter("device_4_green", _parameters.deviceG[3]);
    _parameters.deviceG[3] = _node.get_parameter("device_4_green").as_string();

    device_descriptor.name = "device_4_blue";
    device_descriptor.description = "Path to the device file for the blue led of the fourth indicator";
    _node.declare_parameter("device_4_blue", _parameters.deviceB[3]);
    _parameters.deviceB[3] = _node.get_parameter("device_4_blue").as_string();
}

void LedsDriver::initialize_subscriptions()
{
    _leds_sub = _node.create_subscription<microrobot_interfaces::msg::Leds>(
        "leds", 10, std::bind(&LedsDriver::leds_callback, this, std::placeholders::_1));
}

void LedsDriver::initialize_services()
{
    _set_led_mode_srv = _node.create_service<microrobot_interfaces::srv::SetLedMode>(
        "set_led_mode", std::bind(&LedsDriver::set_led_mode_callback, this, std::placeholders::_1, std::placeholders::_2, std::placeholders::_3));
}

void LedsDriver::setManualMode()
{
    for (int i = 0; i < 4; i++)
    {
        setLedTrigger(_parameters.deviceR[i], "none");
        setLedTrigger(_parameters.deviceG[i], "none");
        setLedTrigger(_parameters.deviceB[i], "none");
    }
}

void LedsDriver::setAutomaticMode()
{
    for (int i = 0; i < 4; i++)
    {
        setLedTrigger(_parameters.deviceR[i], "none");
        setLedTrigger(_parameters.deviceG[i], "pattern");
        setLedPattern(_parameters.deviceG[i], "16 1000 255 1000");
        setLedTrigger(_parameters.deviceB[i], "none");
    }
}

void LedsDriver::setLedColor(int led, uint8_t r, uint8_t g, uint8_t b)
{
    assert(led >= 0 && led < 4);

    setLedBrightness(_parameters.deviceR[led], r);
    setLedBrightness(_parameters.deviceG[led], g);
    setLedBrightness(_parameters.deviceB[led], b);
}

void LedsDriver::leds_callback(const microrobot_interfaces::msg::Leds::SharedPtr msg)
{
    for (int i = 0; i < 4; ++i)
    {
        setLedColor(i, msg->leds[i].r, msg->leds[i].g, msg->leds[i].b);
    }
}

void LedsDriver::set_led_mode_callback(const std::shared_ptr<rmw_request_id_t> request_header,
                                       const std::shared_ptr<microrobot_interfaces::srv::SetLedMode::Request> request,
                                       const std::shared_ptr<microrobot_interfaces::srv::SetLedMode::Response> response)
{
    (void)request_header;

    // Assume success
    response->success = true;
    response->message = "Success";

    switch (request->mode)
    {
    case microrobot_interfaces::srv::SetLedMode::Request::MODE_MANUAL:
        setManualMode();
        break;
    case microrobot_interfaces::srv::SetLedMode::Request::MODE_AUTO:
        setAutomaticMode();
        break;
    default:
        // Invalid mode, error out
        RCLCPP_ERROR(_node.get_logger(), "Invalid led mode: %d", request->mode);
        response->success = false;
        response->message = "Invalid led mode";
        return;
    }
}
