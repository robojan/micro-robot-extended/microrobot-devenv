#include <microrobot_ll/MotorDriver.hpp>
#include <unistd.h>
#include <fcntl.h>

MotorDriver::MotorDriver(rclcpp::Node& node) : _node(node)
{
    initialize_parameters();
    initialize_subscriptions();
    initialize_device();
}

void MotorDriver::initialize_parameters()
{
    rcl_interfaces::msg::ParameterDescriptor device_descriptor;
    device_descriptor.name = "device";
    device_descriptor.type = rcl_interfaces::msg::ParameterType::PARAMETER_STRING;
    device_descriptor.description = "Path to the device file for the motor driver";
    _node.declare_parameter("device", _parameters.device);
    _parameters.device = _node.get_parameter("device").as_string();
}

void MotorDriver::initialize_subscriptions()
{
    _cmd_vel_sub = _node.create_subscription<geometry_msgs::msg::Twist>(
        "cmd_vel", 10, std::bind(&MotorDriver::cmd_vel_callback, this, std::placeholders::_1));
}

void MotorDriver::initialize_device()
{
    _fd = open(_parameters.device.c_str(), O_WRONLY);
    if (_fd < 0)
    {
        RCLCPP_ERROR(_node.get_logger(), "Failed to open motor driver %s: %s", _parameters.device.c_str(),
                     strerror(errno));
        throw std::runtime_error("Failed to open motor driver");
    }
}

void MotorDriver::cmd_vel_callback(const geometry_msgs::msg::Twist::SharedPtr msg)
{
    // Motor 1: rear left reversed
    // Motor 2: rear right
    // Motor 3: front right
    // Motor 4: front left reversed

    float leftSpeed = -(msg->linear.x - msg->angular.z);
    float rightSpeed = msg->linear.x + msg->angular.z;

    set_motor_speeds(leftSpeed, rightSpeed, rightSpeed, leftSpeed);
}

void MotorDriver::set_motor_speeds(float m1, float m2, float m3, float m4)
{
    assert(_fd > 0);

    auto floatToUint16 = [](float f) -> int16_t {
        f = std::clamp(f, -1.0f, 1.0f);
        return static_cast<int16_t>(f * 4095);
    };

    int16_t buffer[4] = { floatToUint16(m1), floatToUint16(m2), floatToUint16(m3), floatToUint16(m4) };

    int written = write(_fd, buffer, sizeof(buffer));
    if (written != sizeof(buffer))
    {
        RCLCPP_ERROR_THROTTLE(_node.get_logger(), *_node.get_clock(), 1000, "Failed to write to motor driver: %s",
                              strerror(errno));
    }
}
