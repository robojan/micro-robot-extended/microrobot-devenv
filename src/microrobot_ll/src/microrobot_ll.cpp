#include <cstdio>
#include <rclcpp/rclcpp.hpp>
#include <microrobot_ll/MotorDriver.hpp>
#include <microrobot_ll/LedsDriver.hpp>
#include <microrobot_ll/Camera.hpp>
#include <microrobot_ll/Iio3AxisSensor.hpp>
#include <iiopp.h>
#include <sensor_msgs/msg/imu.hpp>
#include <sensor_msgs/msg/magnetic_field.hpp>
#include <mutex>
#include <optional>

class MicroRobotLL : public rclcpp::Node
{
  public:
    MicroRobotLL();
    virtual ~MicroRobotLL() = default;

  private:
    struct Parameters
    {
        std::string accelerometer_name = "icm42688p-accel";
        std::string gyroscope_name = "icm42688p-gyro";
        std::string imu_frame = "base_link";
        float imu_rate = 100.0f;

        std::string magnetometer_name = "lis3mdl";
        int magnetometer_rate = 40;
        std::string magnetometer_frame = "base_link";
    };

    struct ImuData
    {
        std::array<double, 3> acceleration;
        std::array<double, 3> angular_velocity;
        bool has_acceleration = false;
        bool has_angular_velocity = false;
        int64_t timestamp = 0;
        std::mutex mutex;

        void reset()
        {
            has_acceleration = false;
            has_angular_velocity = false;
            timestamp = 0;
        }

        bool isComplete() const
        {
            return has_acceleration && has_angular_velocity;
        }
    };

    Parameters _parameters;

    iiopp::ContextPtr _context{ iiopp::create_context(nullptr, nullptr) };
    MotorDriver _motor_driver{ *this };
    LedsDriver _leds_driver{ *this };
    std::optional<Camera> _camera{ *this };
    std::optional<Iio3AxisSensor> _accel_driver;
    std::optional<Iio3AxisSensor> _gyro_driver;
    rclcpp::Publisher<sensor_msgs::msg::Imu>::SharedPtr _imu_pub;
    ImuData _imu_data;
    std::optional<Iio3AxisSensor> _mag_driver;
    rclcpp::Publisher<sensor_msgs::msg::MagneticField>::SharedPtr _mag_pub;

    void initialize_parameters();
    void initialize_imu();
    void initialize_accelerometer();
    void initialize_gyroscope();
    void initialize_magnetometer();

    void publish_imu();
};

MicroRobotLL::MicroRobotLL() : Node("microrobot_ll")
{
    initialize_parameters();
    initialize_imu();
    // initialize_magnetometer();
}

void MicroRobotLL::initialize_parameters()
{
    rcl_interfaces::msg::ParameterDescriptor desc;

    desc.type = rcl_interfaces::msg::ParameterType::PARAMETER_STRING;
    desc.name = "accelerometer_name";
    desc.description = "Name of the accelerometer device";
    declare_parameter("accelerometer_name", _parameters.accelerometer_name, desc);
    _parameters.accelerometer_name = get_parameter("accelerometer_name").as_string();

    desc.type = rcl_interfaces::msg::ParameterType::PARAMETER_STRING;
    desc.name = "gyroscope_name";
    desc.description = "Name of the gyroscope device";
    declare_parameter("gyroscope_name", _parameters.gyroscope_name, desc);
    _parameters.gyroscope_name = get_parameter("gyroscope_name").as_string();

    desc.type = rcl_interfaces::msg::ParameterType::PARAMETER_DOUBLE;
    desc.name = "imu_rate";
    desc.description = "Rate at which the IMU should be read";
    declare_parameter("imu_rate", _parameters.imu_rate, desc);
    _parameters.imu_rate = get_parameter("imu_rate").as_double();

    desc.type = rcl_interfaces::msg::ParameterType::PARAMETER_STRING;
    desc.name = "imu_frame";
    desc.description = "Frame ID for the IMU";
    declare_parameter("imu_frame", _parameters.imu_frame, desc);
    _parameters.imu_frame = get_parameter("imu_frame").as_string();

    desc.type = rcl_interfaces::msg::ParameterType::PARAMETER_STRING;
    desc.name = "magnetometer_name";
    desc.description = "Name of the magnetometer device";
    declare_parameter("magnetometer_name", _parameters.magnetometer_name, desc);
    _parameters.magnetometer_name = get_parameter("magnetometer_name").as_string();

    desc.type = rcl_interfaces::msg::ParameterType::PARAMETER_INTEGER;
    desc.name = "magnetometer_rate";
    desc.description = "Rate at which the magnetometer should be read";
    declare_parameter("magnetometer_rate", _parameters.magnetometer_rate, desc);
    _parameters.magnetometer_rate = get_parameter("magnetometer_rate").as_int();

    desc.type = rcl_interfaces::msg::ParameterType::PARAMETER_STRING;
    desc.name = "magnetometer_frame";
    desc.description = "Frame ID for the magnetometer";
    declare_parameter("magnetometer_frame", _parameters.magnetometer_frame, desc);
    _parameters.magnetometer_frame = get_parameter("magnetometer_frame").as_string();
}

void MicroRobotLL::initialize_imu()
{
    auto qos = rclcpp::SensorDataQoS();
    _imu_pub = create_publisher<sensor_msgs::msg::Imu>("imu", qos);

    initialize_accelerometer();
    initialize_gyroscope();
}

void MicroRobotLL::initialize_accelerometer()
{
    std::unordered_map<std::string, std::string> parameters;
    parameters["sampling_frequency"] = std::to_string(_parameters.imu_rate);

    _accel_driver.emplace(*this, *_context, _parameters.accelerometer_name, parameters,
                          [this](const Iio3AxisSensor::DataSample& sample) {
                              std::lock_guard lock(_imu_data.mutex);
                              if (_imu_data.timestamp != 0 &&
                                  abs(sample.timestamp - _imu_data.timestamp) > 1e9f / (_parameters.imu_rate / 2))
                              {
                                  _imu_data.reset();
                              }
                              _imu_data.timestamp = sample.timestamp;
                              _imu_data.acceleration = { sample.value[0], sample.value[1], sample.value[2] };
                              _imu_data.has_acceleration = true;
                              publish_imu();
                          });
}

void MicroRobotLL::initialize_gyroscope()
{
    std::unordered_map<std::string, std::string> parameters;
    parameters["sampling_frequency"] = std::to_string(_parameters.imu_rate);

    _gyro_driver.emplace(*this, *_context, _parameters.gyroscope_name, parameters,
                         [this](const Iio3AxisSensor::DataSample& sample) {
                             std::lock_guard lock(_imu_data.mutex);
                             if (_imu_data.timestamp != 0 &&
                                 abs(sample.timestamp - _imu_data.timestamp) > 1e9f / (_parameters.imu_rate / 2))
                             {
                                 _imu_data.reset();
                             }
                             _imu_data.timestamp = sample.timestamp;
                             _imu_data.angular_velocity = { sample.value[0], sample.value[1], sample.value[2] };
                             _imu_data.has_angular_velocity = true;
                             publish_imu();
                         });
}

void MicroRobotLL::publish_imu()
{
    if (_imu_data.isComplete())
    {
        auto msg = sensor_msgs::msg::Imu();
        msg.header.stamp = rclcpp::Time(_imu_data.timestamp);
        msg.header.frame_id = _parameters.imu_frame;
        msg.orientation_covariance[0] = -1;
        msg.linear_acceleration.x = _imu_data.acceleration[0];
        msg.linear_acceleration.y = _imu_data.acceleration[1];
        msg.linear_acceleration.z = _imu_data.acceleration[2];
        msg.angular_velocity.x = _imu_data.angular_velocity[0];
        msg.angular_velocity.y = _imu_data.angular_velocity[1];
        msg.angular_velocity.z = _imu_data.angular_velocity[2];
        _imu_pub->publish(msg);
        _imu_data.reset();
    }
}

void MicroRobotLL::initialize_magnetometer()
{
    auto qos = rclcpp::SensorDataQoS();
    _mag_pub = create_publisher<sensor_msgs::msg::MagneticField>("magnetic_field", qos);

    std::unordered_map<std::string, std::string> parameters;
    parameters["sampling_frequency"] = std::to_string(_parameters.magnetometer_rate);

    _mag_driver.emplace(*this, *_context, _parameters.magnetometer_name, parameters,
                        [this](const Iio3AxisSensor::DataSample& sample) {
                            auto msg = sensor_msgs::msg::MagneticField();
                            msg.header.stamp = rclcpp::Time(sample.timestamp);
                            msg.header.frame_id = _parameters.magnetometer_frame;
                            msg.magnetic_field.x = sample.value[0];
                            msg.magnetic_field.y = sample.value[1];
                            msg.magnetic_field.z = sample.value[2];
                            _mag_pub->publish(msg);
                        });
}

int main(int argc, char** argv)
{
    rclcpp::init(argc, argv);

    auto node = std::make_shared<MicroRobotLL>();
    rclcpp::spin(node);

    rclcpp::shutdown();
    return 0;
}
