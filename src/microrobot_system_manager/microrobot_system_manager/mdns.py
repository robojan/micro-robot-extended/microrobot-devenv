import threading
import dbus
import dbus.mainloop.glib
from gi.repository import GLib

class MdnsServiceAdvertiser:
    def __init__(self):
        self.mainloop = None
        self.group = None
        self.services = []
        self.thread = None

        # Start the mainloop
        dbus.mainloop.glib.DBusGMainLoop(set_as_default=True)
        self.mainloop = GLib.MainLoop()
        self.thread = threading.Thread(target=self.mainloop.run)
        self.thread.start()

    def advertise_mdns_service(self, service_name, service_type, port, txt_records=None):

        bus = dbus.SystemBus()
        server = dbus.Interface(bus.get_object('org.freedesktop.Avahi', '/'),
                                'org.freedesktop.Avahi.Server')

        if not self.group:
            self.group = dbus.Interface(bus.get_object('org.freedesktop.Avahi',
                                                       server.EntryGroupNew()),
                                        'org.freedesktop.Avahi.EntryGroup')

        if txt_records:
            # Convert each key-pair value to a bytearray
            txt = [f"{key}={value}".encode() for key, value in txt_records.items()]
        else:
            txt = []

        self.group.AddService(-1, -1, dbus.UInt32(0), service_name, service_type,
                             '', '', port, txt)
    
        self.services.append(service_name)
    
    def commit_mdns_service(self):
        self.group.Commit()


    def stop_mdns_service(self):
        if self.mainloop:
            self.mainloop.quit()
        if self.group:
            self.group.Reset()
            self.group = None
        if self.thread and self.thread.is_alive():
            self.thread.join()

