import rclpy
from rclpy.node import Node
from microrobot_system_manager.system_manager_node import SystemManagerNode
import logging

class RosLogHandler(logging.Handler):
    def __init__(self, node: Node, level=logging.NOTSET):
        super().__init__(level)
        self.__ros_logger = node.get_logger()

    def emit(self, record: logging.LogRecord):
        match(record.levelno):
            case logging.DEBUG:
                self.__ros_logger.debug(str(record.msg) % record.args)
            case logging.INFO:
                self.__ros_logger.info(str(record.msg) % record.args)
            case logging.WARNING:
                self.__ros_logger.warning(str(record.msg) % record.args)
            case logging.ERROR:
                self.__ros_logger.error(str(record.msg) % record.args)
            case logging.CRITICAL:
                self.__ros_logger.critical(str(record.msg) % record.args)
            case _:
                self.__ros_logger.warning(str(record.msg) % record.args)

def main(args=None):
    rclpy.init(args=args)

    node = SystemManagerNode()
    ros_log_handler = RosLogHandler(node)
    logging.getLogger().addHandler(ros_log_handler)
    logging.getLogger().setLevel(logging.DEBUG)
    
    rclpy.spin(node)

    node.stop()
    
    rclpy.shutdown()


if __name__ == '__main__':
    main()
