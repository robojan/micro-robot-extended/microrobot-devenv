from rclpy.node import Node, ParameterDescriptor, ParameterType
from rclpy.qos import QoSProfile, QoSDurabilityPolicy, QoSReliabilityPolicy, QoSHistoryPolicy
from microrobot_interfaces.msg import SystemInfo as SystemInfoMsg
from microrobot_system_manager.mdns import MdnsServiceAdvertiser
import microrobot_system_manager.constants as constants

class SystemManagerNode(Node):
    def __init__(self, *args, **kwargs):
        super().__init__('system_manager', *args, **kwargs)

        self.initialize_parameters()
        self.initialize_publishers()

        self.setup_mdns()

        self.publish_system_info()

        self.get_logger().info('Sysinfo node initialized')

    def stop(self):
        self.mdns_advertiser.stop_mdns_service()

    def initialize_parameters(self):
        
        descriptor = ParameterDescriptor(
            name="robot_name",
            type=ParameterType.PARAMETER_STRING,
            description="Name of the robot",
            read_only=True
        )
        self.declare_parameter("robot_name", "microrobot", descriptor)

        descriptor = ParameterDescriptor(
            name="robot_type",
            type=ParameterType.PARAMETER_STRING,
            description="Type of the robot",
            read_only=True
        )
        self.declare_parameter("robot_type", "microrobot", descriptor)

        descriptor = ParameterDescriptor(
            name="serial_number",
            type=ParameterType.PARAMETER_STRING,
            description="Serial number of the robot",
            read_only=True
        )
        self.declare_parameter("serial_number", "P00000000", descriptor)

        descriptor = ParameterDescriptor(
            name="ssh_port",
            type=ParameterType.PARAMETER_INTEGER,
            description="Port of the SSH service, -1 if not exposed",
            read_only=True
        )
        self.declare_parameter("ssh_port", -1, descriptor)

        descriptor = ParameterDescriptor(
            name="rosbridge_port",
            type=ParameterType.PARAMETER_INTEGER,
            description="Port of the ROSBridge service, -1 if not exposed",
            read_only=True
        )
        self.declare_parameter("rosbridge_port", -1, descriptor)

        descriptor = ParameterDescriptor(
            name="http_port",
            type=ParameterType.PARAMETER_INTEGER,
            description="Port of the HTTP service, -1 if not exposed",
            read_only=True
        )
        self.declare_parameter("http_port", -1, descriptor)

        descriptor = ParameterDescriptor(
            name="foxglove_port",
            type=ParameterType.PARAMETER_INTEGER,
            description="Port of the Foxglove service, -1 if not exposed",
            read_only=True
        )
        self.declare_parameter("foxglove_port", -1, descriptor)

        descriptor = ParameterDescriptor(
            name="zenoh_port",
            type=ParameterType.PARAMETER_INTEGER,
            description="Port of the Zenoh service, -1 if not exposed",
            read_only=True
        )
        self.declare_parameter("zenoh_port", -1, descriptor)

    def initialize_publishers(self):
        low_rate_qos = QoSProfile(
            depth=3,
            history=QoSHistoryPolicy.KEEP_LAST,
            durability=QoSDurabilityPolicy.TRANSIENT_LOCAL,
            reliability=QoSReliabilityPolicy.RELIABLE
        )
        self.system_info_publisher = self.create_publisher(SystemInfoMsg, 'system_info', low_rate_qos)

    def publish_system_info(self):
        msg = SystemInfoMsg(
            name=self.get_parameter("robot_name").value,
            type=self.get_parameter("robot_type").value,
            serial=self.get_parameter("serial_number").value
        )
        self.system_info_publisher.publish(msg)

    def setup_mdns(self):
        txt_records = {
            constants.MDNS_TYPE_RECORD_NAME: self.get_parameter("robot_type").value,
            constants.MDNS_SERIAL_RECORD_NAME: self.get_parameter("serial_number").value,
            constants.MDNS_NAME_RECORD_NAME: self.get_parameter("robot_name").value
        }
        name = constants.MDNS_NAME_TEMPLATE.format(
            name=self.get_parameter("robot_name").value, 
            serial=self.get_parameter("serial_number").value, 
            type=self.get_parameter("robot_type").value
        )

        self.mdns_advertiser = MdnsServiceAdvertiser()
        if self.get_parameter("ssh_port").value != -1:
            self.mdns_advertiser.advertise_mdns_service(
                service_name=name,
                service_type='_ssh._tcp',
                port=self.get_parameter("ssh_port").value,
                txt_records=txt_records
            )
        if self.get_parameter("http_port").value != -1:
            self.mdns_advertiser.advertise_mdns_service(
                service_name=name,
                service_type="_http._tcp",
                port=self.get_parameter("http_port").value,
                txt_records=txt_records
            )

        if self.get_parameter("rosbridge_port").value != -1:
            self.mdns_advertiser.advertise_mdns_service(
                service_name=name,
                service_type="_rosbridge._tcp",
                port=self.get_parameter("rosbridge_port").value,
                txt_records=txt_records
            )

        if self.get_parameter("foxglove_port").value != -1:
            self.mdns_advertiser.advertise_mdns_service(
                service_name=name,
                service_type="_foxglove._tcp",
                port=self.get_parameter("foxglove_port").value,
                txt_records=txt_records
            )

        if self.get_parameter("zenoh_port").value != -1:
            self.mdns_advertiser.advertise_mdns_service(
                service_name=name,
                service_type="_zenoh._tcp",
                port=self.get_parameter("zenoh_port").value,
                txt_records=txt_records
            )

        self.mdns_advertiser.commit_mdns_service()
