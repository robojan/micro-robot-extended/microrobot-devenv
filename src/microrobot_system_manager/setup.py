from setuptools import setup
import sys
import os

package_name = 'microrobot_system_manager'

# We override the python executable if this is specified in the environment.
# We need to to this to make sure that the shebang line in the generated
# scripts is correct. If we don't do this, the shebang line will point to the
# python interpreter in the build environment, which is not what we want.
# The interpreter that boa will set will contain a magic string that will be
# replaced by the correct interpreter by the boa post-processing script.
if 'OVERRIDE_PYTHON_EXECUTABLE' in os.environ:
    sys.executable = os.environ['OVERRIDE_PYTHON_EXECUTABLE']

setup(
    name=package_name,
    version='1.0.0',
    packages=[package_name],
    data_files=[
        ('share/ament_index/resource_index/packages',
            ['resource/' + package_name]),
        ('share/' + package_name, ['package.xml']),
    ],
    install_requires=['setuptools'],
    zip_safe=True,
    maintainer='Robbert-Jan de Jager',
    maintainer_email='robojan1@hotmail.com',
    description='System manager library for the microrobot',
    license='MIT',
    tests_require=['pytest'],
    entry_points={
        'console_scripts': [
            'microrobot_system_manager = microrobot_system_manager.microrobot_system_manager:main'
        ],
    },
)
